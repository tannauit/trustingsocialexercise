require 'spec_helper'

describe Exercise::Pair do
  let!(:second) { rand(100) }
  let!(:value) { rand(1000) }
  subject { described_class.new(second, value) }

  it { should respond_to :second }
  it { should respond_to :value }

  it 'should have to_json method' do
    JSON.parse(subject.to_json()).should eq({ 'value' => value, 'second' => second })
  end
end
