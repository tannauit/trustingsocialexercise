require 'spec_helper'

describe Exercise::Node do
  let!(:second) { rand(100) }
  let!(:value) { rand(1000) }
  let(:pair) { Exercise::Pair.new(second, value) }
  let(:list) { Exercise::LinkedList.new }
  subject { described_class.new(pair, nil, nil, list) }

  it { should respond_to :next }
  it { should respond_to :prev }
  it { should respond_to :list }
  it { should respond_to :pair }
  it { should respond_to :next= }
  it { should respond_to :prev= }

  it "should call tick_expire" do
    expect_any_instance_of(Exercise::Node).to receive(:tick_expire)
    Exercise::Node.new(pair, nil, nil, list)
    sleep(1)
  end
end
