require 'spec_helper'

describe Exercise::LinkedList do

  it { should respond_to :count }

  context 'empty' do
    let!(:list) { Exercise::LinkedList.new }

    it 'should add new node successfully' do
      pair = Exercise::Pair.new(rand(100), rand(1000))
      expect{list.push(pair)}.to change{list.count}.from(0).to(1)
    end
  end

  context 'one element' do
    let!(:list) { Exercise::LinkedList.new }

    before do
      pair = Exercise::Pair.new(rand(100), rand(1000))
      list.push(pair)
    end

    it 'should add new node successfully' do
      pair = Exercise::Pair.new(rand(100), rand(1000))
      expect{list.push(pair)}.to change{list.count}.from(1).to(2)
    end

    it 'should delete node successfully' do
      expect{list.shift}.to change{list.count}.from(1).to(0)
    end
  end

  context 'full' do
    let!(:list) { Exercise::LinkedList.new }
    let(:pair) { Exercise::Pair.new(rand(100), rand(1000)) }

    before do
      list.push(pair)
      stub_const("Exercise::LinkedList::MAX_LENGTH", 1)
    end

    it 'should not change the size when push' do
      expect{list.push(pair)}.not_to change{list.count}
    end

    it 'should remove the oldest node' do
      expect(list).to receive(:delete_node)
      list.push(pair)
    end
  end
end
