class MainController < ApplicationController
  layout "main"

  def index
    @main_props = {
      sum: EXERCISE.sum_oldest_10,
      median: EXERCISE.median
    }
  end

  def latest_pairs
    number_pair = params[:number_pair].to_i

    return render status: :unprocessable_entity, json: { messages: ['Missing number_pair param'] } if number_pair == 0

    pairs = EXERCISE.list.get_tail(number_pair)
    render json: pairs, root: nil, each_serializer: PairSerializer
  end

  def sum_oldest_10
    render json: { sum: EXERCISE.sum_oldest_10 }
  end

  def median
    render json: { median: EXERCISE.median }
  end
end
