class PairSerializer < ActiveModel::Serializer
  attributes :value, :second
end
