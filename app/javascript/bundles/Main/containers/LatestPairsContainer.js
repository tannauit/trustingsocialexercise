// @flow

import { connect } from 'react-redux'
import LatestPairsComponent from '../components/LatestPairs.jsx'
import type { Pair } from '../types/global'

const mapStateToProps = function(state: State): { pairs: Array<Pair> } {
  return { pairs: state.latestPairs }
}

const mapFunctions = {}

export default connect(mapStateToProps, mapFunctions)(LatestPairsComponent)
