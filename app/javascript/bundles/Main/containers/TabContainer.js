// @flow

import { connect } from 'react-redux'
import Tab from '../components/Tab.jsx'
import type { State } from '../types/global'
import { handleMenuChanged } from '../modules/initialReducer'

const mapStateToProps = function(state: State): { activeMenu: string } {
  return { activeMenu: state.activeMenu }
}

const mapFunctions = {
  handleMenuChanged
}

export default connect(mapStateToProps, mapFunctions)(Tab)
