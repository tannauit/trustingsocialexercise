// @flow

import { connect } from 'react-redux'
import TabManager from '../components/TabManager.jsx'
import type { State } from '../types/global'

const mapStateToProps = function(state: State): { activeMenu: string } {
  return {
    activeMenu: state.activeMenu,
    activeContext: state.activeContext
  }
}

const mapFunctions = {}

export default connect(mapStateToProps, mapFunctions)(TabManager)
