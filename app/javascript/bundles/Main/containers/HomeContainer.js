// @flow

import { connect } from 'react-redux'
import Home from '../components/Home.jsx'
import type { State } from '../types/global'
import {
  handleDisplaySum,
  handleDisplayMedian
} from '../modules/initialReducer'

const mapStateToProps = function(
  state: State
): {
  sum: number,
  median: number
} {
  return {
    sum: state.sum,
    median: state.median
  }
}

const mapFunctions = {
  handleDisplaySum,
  handleDisplayMedian
}

export default connect(mapStateToProps, mapFunctions)(Home)
