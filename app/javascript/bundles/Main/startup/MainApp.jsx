export default process.env.NODE_ENV === 'production' ?
  require('./MainApp.prod.jsx').default : require('./MainApp.dev.jsx').default;
