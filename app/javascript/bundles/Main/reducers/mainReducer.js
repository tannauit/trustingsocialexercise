// @flow
import { combineReducers } from 'redux'

import initialReducer from '../modules/initialReducer'
import latestPairs from '../modules/pairReducer'
import {
  errorMessages,
  noticeMessages
} from '../modules/notifyMessagesReducers'

const mainReducer = combineReducers({
  ...initialReducer,
  latestPairs: latestPairs,
  errorMessages: errorMessages,
  noticeMessages: noticeMessages
})

export default mainReducer
