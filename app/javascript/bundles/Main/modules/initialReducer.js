// @flow

import {
  handleShowErrorMessages,
  handleShowNoticeMessages
} from '../modules/notifyMessagesReducers.js'
import { loadLatestPairs } from '../modules/pairReducer.js'
import { APIUtil } from '../../../packs/utils/api_util'

type Action =
  | { type: 'MENU_CHANGED', payload: string }
  | { type: 'SUM_CHANGED', payload: number }
  | { type: 'MEDIAN_CHANGED', payload: number }

export function handleMenuChanged(menuName: string): ThunkAction {
  return function(dispatch: Dispatch, getState: GetState) {
    dispatch(handleShowErrorMessages([]))
    dispatch(handleShowNoticeMessages([]))
    dispatch({ type: 'MENU_CHANGED', payload: menuName })
    if (menuName === 'latestPairs') {
      dispatch(loadLatestPairs(20))
    }
  }
}

export function handleSumChanged(sum: number): Action {
  return {
    type: 'SUM_CHANGED',
    payload: sum
  }
}

export function handleDisplaySum(): ThunkAction {
  return function(dispatch: Dispatch) {
    APIUtil.displaySum().then((responseData: ResponseData) => {
      if (responseData.success) {
        dispatch(handleSumChanged(responseData.data.sum))
      } else {
        dispatch(handleShowErrorMessages(responseData.messages))
      }
    })
  }
}

export function handleMedianChanged(median: number): Action {
  return {
    type: 'MEDIAN_CHANGED',
    payload: median
  }
}

export function handleDisplayMedian(): ThunkAction {
  return function(dispatch: Dispatch) {
    APIUtil.displayMedian().then((responseData: ResponseData) => {
      if (responseData.success) {
        dispatch(handleMedianChanged(responseData.data.median))
      } else {
        dispatch(handleShowErrorMessages(responseData.messages))
      }
    })
  }
}

const reducers = {}

reducers.median = (state: number = 0, action: Action): number => {
  switch (action.type) {
    case 'MEDIAN_CHANGED':
      return action.payload
    default:
      return state
  }
}

reducers.sum = (state: number = 0, action: Action): number => {
  switch (action.type) {
    case 'SUM_CHANGED':
      return action.payload
    default:
      return state
  }
}

reducers.activeMenu = (state: string = 'main', action: Action): string => {
  switch (action.type) {
    case 'MENU_CHANGED':
      return action.payload
    default:
      return state
  }
}
export default reducers
