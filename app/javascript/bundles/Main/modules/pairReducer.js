// @flow

import type { Pair, ResponseData } from '../types/global'
import type { Dispatch, ThunkAction } from '../types/redux'
import { APIUtil } from '../../../packs/utils/api_util'
import { handleShowErrorMessages } from '../modules/notifyMessagesReducers.js'

type Action = { type: 'UPDATE_LATEST_PAIRS_UI', payload: Array<Pair> }

export function handleLatestPairsLoaded(pairs: Array<Pair>): Action {
  return {
    type: 'UPDATE_LATEST_PAIRS_UI',
    payload: pairs
  }
}

export function loadLatestPairs(numberPair: number): ThunkAction {
  return function(dispatch: Dispatch) {
    APIUtil.loadLatestPairs(numberPair).then((responseData: ResponseData) => {
      if (responseData.success) {
        dispatch(handleLatestPairsLoaded(responseData.data))
      } else {
        dispatch(handleShowErrorMessages(responseData.messages))
      }
    })
  }
}

export default (state: Array<Pair> = [], action: Action): Array<Pair> => {
  switch (action.type) {
    case 'UPDATE_LATEST_PAIRS_UI':
      return action.payload
    default:
      return state
  }
}
