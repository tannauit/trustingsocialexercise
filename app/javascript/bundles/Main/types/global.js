// @flow

export type Pair = {
  value: number,
  second: number
}

export type State = {
  latestPairs: Array<Pair>,
  activeMenu: string,
  sum: number,
  median: number
}

export type ResponseData = {
  success: boolean,
  messages: Array<string>,
  data: any
}
