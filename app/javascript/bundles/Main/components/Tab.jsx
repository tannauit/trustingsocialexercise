// @flow
import React from 'react';

type Props = {
  activeMenu: string,
  handleMenuChanged: (menuName: string) => {}
};

export default class Tab extends React.Component<Props> {
  render() {
    return (
     <nav className="navbar navbar-default">
      <div className="container-fluid">
        <div className="navbar-header">
          <a className="navbar-brand" href="#">TSE</a>
        </div>

        <div className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li className={this.props.activeMenu == 'main' ? 'active': ''}><a href="#" onClick={this.props.handleMenuChanged.bind(this, 'main')}>Exercise</a></li>
            <li className={this.props.activeMenu == 'latestPairs' ? 'active': ''}><a href="#" onClick={this.props.handleMenuChanged.bind(this, 'latestPairs')}>Latest Pairs</a></li>
          </ul>
        </div>
      </div>
    </nav>
    );
  }
}
