// @flow
import React from 'react';
import TabContainer from '../containers/TabContainer'
import NotifyMessagesContainer from '../containers/NotifyMessagesContainer'
import TabManagerContainer from '../containers/TabManagerContainer'

type Props = {
};

export default class Main extends React.Component<Props> {
  render() {
    return <div>
      <TabContainer />
      <NotifyMessagesContainer />
      <TabManagerContainer />
    </div>;
  }
}
