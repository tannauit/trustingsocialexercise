
// @flow
import React from 'react';
import HomeContainer from '../containers/HomeContainer'
import LatestPairsContainer from '../containers/LatestPairsContainer'

type Props = {
  activeMenu: string
};

export default class TabManager extends React.Component<Props> {
  render() {
    return <div className="container">
      {this.contentElement()}
    </div>
  }

  contentElement() {
    if (this.props.activeMenu === 'main') {
      return <HomeContainer />
    } else if (this.props.activeMenu === 'latestPairs'){
      return <LatestPairsContainer />
    }
  }
}
