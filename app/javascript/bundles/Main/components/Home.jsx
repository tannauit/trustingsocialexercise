// @flow
import React from 'react';
import TabContainer from '../containers/TabContainer'
import styles from '../styles/main.scss'

type Props = {
  sum: number,
  median: number,
  handleDisplaySum: () => {},
  handleDisplayMedian: () => {}
};

export default class Main extends React.Component<Props> {
  render() {
    return <div>
      <div>
        <button onClick={ this.props.handleDisplaySum.bind(this) }>
          Display Sum
        </button>
        <div>
          <label className={ styles.label }> Sum: </label>
          <span>{ this.props.sum }</span>
        </div>
      </div>
      <div>
        <button onClick={ this.props.handleDisplayMedian.bind(this) }>
          Display Median
        </button>
        <div>
          <label className={ styles.label }> Median: </label>
          <span>{ this.props.median }</span>
        </div>
      </div>
    </div>;
  }
}
