// @flow
import React from 'react';
import type { Pair } from '../types/global'

type Props = {
  pairs: Array<Pair>
};

export default class Main extends React.Component<Props> {
  render() {
    return <div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Second</th>
            <th scope="col">Value</th>
          </tr>
        </thead>
        <tbody>
        {this.props.pairs.map((pair: Pair, index: number) => {
          return <tr key={ index } scope="row">
            <td> { index + 1 } </td>
            <td>{ pair.second } </td>
            <td>{ pair.value } </td>
          </tr>
        })}
        </tbody>
      </table>
    </div>;
  }
}
