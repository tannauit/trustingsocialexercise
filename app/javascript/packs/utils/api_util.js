import request from 'superagent'

export const APIUtil = {
  loadLatestPairs: numberPair => {
    return request
      .get(`/main/latest_pairs?number_pair=${numberPair}`)
      .send()
      .then(response => {
        return { success: true, data: response.body }
      })
      .catch(function(err) {
        return { success: false, messages: err.response.body.messages }
      })
  },
  displaySum: () => {
    return request
      .get('/main/sum_oldest_10')
      .send()
      .then(response => {
        return { success: true, data: response.body }
      })
      .catch(function(err) {
        return { success: false, messages: err.response.body.messages }
      })
  },
  displayMedian: () => {
    return request
      .get('/main/median')
      .send()
      .then(response => {
        return { success: true, data: response.body }
      })
      .catch(function(err) {
        return { success: false, messages: err.response.body.messages }
      })
  }
}
