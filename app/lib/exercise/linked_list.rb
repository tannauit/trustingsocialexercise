module Exercise
  class LinkedList
    attr_accessor :count

    MAX_LENGTH = 10000

    def initialize()
      @head = nil
      @tail = nil
      @count = 0
    end

    def push(pair)
      append(pair)
      shift() if @count > MAX_LENGTH
    end

    def shift
      delete_node(@head)
    end

    def delete_node(node)
      return if @head.nil? || @tail.nil?
      puts "deleting node"

      @count -= 1
      if node == @head && node == @tail
        @head = nil
        @tail = nil
      elsif node == @head
        @head = @head.next
        @head.prev = nil
      elsif node == @tail
        @tail = @tail.prev
        @tail.next = nil
      else
        next_node = node.next
        prev_node = node.prev
        next_node.prev = node.prev
        prev_node.next = node.next
      end
      node.clear_pointer
    end

    def get_head(num)
      items = []
      current = @head
      index = 0
      while(current.present?) do
        items << current.pair
        index += 1
        break if index == num

        current = current.next
      end
      items
    end

    def get_tail(num)
      items = []
      current = @tail
      index = 0
      while(current.present?) do
        items << current.pair
        index += 1
        break if index == num

        current = current.prev
      end
      items
    end

    private
    def prepend(pair)
      @count++

        if @head.nil?
          @head = Node.new(pair, nil, nil, self)
          @tail = @head
      else
        new_node = Node.new(pair, @head, nil, self)
        @head.prev = new_node
        @head = new_noe
      end
    end

    def append(pair)
      @count += 1

      if @tail.nil?
        @tail = Node.new(pair, nil, nil, self)
        @head = @tail
      else
        new_node = Node.new(pair, nil, @tail, self)
        @tail.next = new_node
        @tail = new_node
      end
    end
  end
end
