module Exercise
  class Main
    attr_accessor :list

    def initialize()
      @list = LinkedList.new
      @sum_list = []
    end

    def start
      task = Concurrent::TimerTask.new(execution_interval: 1) do
        random_pair
        add_to_list
      end
      task.execute
    end

    def random_pair
      @second = rand(100) + 1
      @value = rand(9000) + 1001
    end

    def add_to_list
      pair = Pair.new(@second, @value)
      puts @list.count
      @list.push(pair)
    end

    def sum_oldest_10
      pairs = @list.get_head(10)
      sum = 0
      mutex = Mutex.new
      threads = pairs.map do |pair|
        Thread.new do
          mutex.synchronize {
            sum += pair.value
          }
        end
      end

      threads.map(&:join)
      statuses = threads.map(&:status).uniq
      while (statuses.count != 1 || statuses.first != false) do
        sleep(0.5)
        statuses = threads.map(&:status).uniq
      end
      add_to_sum_list_with_order(sum)
      sum
    end

    def median
      return 0 if @sum_list.empty?

      count = @sum_list.count
      index = count/2
      if (count % 2 == 0)
        (@sum_list[index] + @sum_list[index - 1]) / 2
      else
        @sum_list[index]
      end
    end

    private
    def add_to_sum_list_with_order(number)
      left = 0
      if @sum_list.count > 0
        right = @sum_list.count - 1

        while (left <= right) do
          mid = (left + right) / 2
          if (number == @sum_list[mid])
            left = mid
            break
          elsif (number < @sum_list[mid])
            right = mid - 1;
          elsif (number > @sum_list[mid])
            left = mid + 1;
          end
        end
      end
      @sum_list.insert(left, number)
      puts @sum_list.to_json
    end
  end
end
