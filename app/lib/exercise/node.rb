module Exercise
  class Node
    attr_reader :pair, :list
    attr_accessor :next, :prev

    def initialize(pair, next_node, prev_node, list)
      @pair = pair
      @next = next_node
      @prev = prev_node
      @time_to_live = pair.second
      @list = list

      @task = Concurrent::TimerTask.new(execution_interval: 1) do
        tick_expire
      end
      @task.execute
    end

    def tick_expire
      @time_to_live -= 1
      if @time_to_live <= 0
        puts "expired"
        @task.shutdown
        @list.delete_node(self)
      end
    end

    def clear_pointer
      @list = nil
      @pair = nil
      @prev = nil
      @next = nil
    end
  end
end
