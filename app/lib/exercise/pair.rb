module Exercise
  class Pair
    include ActiveModel::SerializerSupport

    attr_reader :second, :value

    def initialize(second, value)
      @second = second
      @value = value
    end
  end
end
