const ExtractTextPlugin = require('extract-text-webpack-plugin')
const path = require('path')

const isProduction = process.env.NODE_ENV === 'production'

const extractOptions = {
  fallback: 'style-loader',
  use: [
    {
      loader: 'css-loader',
      options: {
        minimize: isProduction,
        modules: true,
        localIdentName: '[name]__[local]--[hash:base64:5]'
      }
    },
    {
      loader: 'sass-loader',
      options: {
        sourceMap: true,
        data: '@import "variables";',
        includePaths: [path.resolve(__dirname, "../../../app/javascript/bundles/main/styles")]
      }
    }
  ]
}

// For production extract styles to a separate bundle
module.exports = {
  test: /\.(scss|sass)$/i,
  use: ExtractTextPlugin.extract(extractOptions)
}
