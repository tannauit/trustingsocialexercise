Rails.application.routes.draw do
  get 'hello_world', to: 'hello_world#index'
  get 'app', to: 'main#index'

  resources :main, only: [:index] do
    collection do
      get :latest_pairs
      get :sum_oldest_10
      get :median
    end
  end
end
