# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

 2.4.0

* Rails version

  5.1.6

* Installing

  1. Install RVM and then install ruby
  2. cd to project directory
  3. run `gem install bundler`
  4. run `bundle install`
  5. run `yarn install`
  6. We can start project now
		1. run js compile by command `yarn js-watch`
		2. Start server by command `rails s`
  7. You can test by go to http://localhost:3000/app


* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
